<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adverts', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid');
            $table->uuid('user_id');
            $table->string('title');
            $table->integer('category_id');
            $table->integer('price_type')->comment('1- Simple, 2- Multiple');
            $table->string('lat')->nullable()->comment('Latitude');
            $table->string('lng')->nullable()->comment('Longitude');
            $table->string('area')->nullable();
            $table->text('description');
            $table->integer('point');
            $table->integer('participant_count')->default(0);
            $table->timestamp('finished_at');
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adverts');
    }
}
