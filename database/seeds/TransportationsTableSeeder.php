<?php

use Illuminate\Database\Seeder;

class TransportationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('transportations')->insert([
            [
                'name'   => 'Depodan',
                'status' => 1
            ],
            [
                'name'   => 'Tarladan',
                'status' => 1
            ],
            [
                'name'   => 'Nakliye Alıcı Öder',
                'status' => 1
            ],
            [
                'name'   => 'Nakliye Satıcı Öder',
                'status' => 1
            ],
            [
                'name'   => 'Kargo Alıcı Öder',
                'status' => 1
            ],
            [
                'name'   => 'Kargo Satıcı Öder',
                'status' => 1
            ],
            [
                'name'   => 'Gemiden',
                'status' => 1
            ],
            [
                'name'   => 'Mağzadan',
                'status' => 1
            ],
        ]);
    }
}
