<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 25 Feb 2019 12:35:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserSetting
 *
 * @property int $id
 * @property string $user_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class UserSetting extends Eloquent
{
    protected $fillable = [
        'user_id'
    ];
}
