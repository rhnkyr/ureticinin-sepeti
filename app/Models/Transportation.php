<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 25 Feb 2019 13:14:11 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Transportation
 *
 * @property int $id
 * @property string $name
 * @property int $status
 * @property int $order
 *
 * @property \Illuminate\Database\Eloquent\Collection $adverts
 *
 * @package App\Models
 */
class Transportation extends Eloquent
{
    public $timestamps = false;

    protected $casts = [
        'status' => 'int',
        'order'  => 'int'
    ];

    protected $fillable = [
        'name',
        'status',
        'order'
    ];

    public function adverts()
    {
        return $this->belongsToMany(\App\Models\Advert::class);
    }
}
