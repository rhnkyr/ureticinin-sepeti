<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 25 Feb 2019 12:35:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Faq
 *
 * @property int $id
 * @property string $title
 * @property string $content
 * @property int $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Faq extends Eloquent
{
    protected $casts = [
        'status' => 'int'
    ];

    protected $fillable = [
        'title',
        'content',
        'status'
    ];
}
