<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 25 Feb 2019 12:35:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Rating
 *
 * @property int $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $rating
 * @property string $rateable_type
 * @property int $rateable_id
 * @property int $user_id
 *
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class Rating extends Eloquent
{
    protected $casts = [
        'rating' => 'int',
        'rateable_id' => 'int',
        'user_id' => 'int'
    ];

    protected $fillable = [
        'rating',
        'rateable_type',
        'rateable_id',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }
}
