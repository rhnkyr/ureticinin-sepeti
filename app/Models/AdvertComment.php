<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 07 Apr 2019 13:29:28 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class AdvertComment
 *
 * @property int $id
 * @property int $advert_id
 * @property int $user_id
 * @property string $content
 * @property int $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class AdvertComment extends Eloquent
{
    protected $casts = [
        'advert_id' => 'int',
        'user_id'   => 'int',
        'status'    => 'int'
    ];

    protected $fillable = [
        'advert_id',
        'user_id',
        'content',
        'status'
    ];

    public function advert()
    {
        return $this->belongsTo(Advert::class);
    }

}
