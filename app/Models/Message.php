<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 25 Feb 2019 12:35:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Message
 *
 * @property int $id
 * @property int $conversation_id
 * @property string $author_id
 * @property string $message
 * @property int $is_read
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Message extends Eloquent
{
    protected $casts = [
        'conversation_id' => 'int',
        'is_read' => 'int'
    ];

    protected $fillable = [
        'conversation_id',
        'author_id',
        'message',
        'is_read'
    ];
}
