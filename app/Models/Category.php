<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 25 Feb 2019 12:35:16 +0000.
 */

namespace App\Models;

use Kalnoy\Nestedset\NodeTrait;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Category
 *
 * @property int $id
 * @property int $parent_id
 * @property string $name
 * @property string $description
 * @property string $image
 * @property int $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Category extends Eloquent
{

    use NodeTrait;

    protected $casts = [
        'parent_id' => 'int',
        'status'    => 'int',
        '_lft'      => 'int',
        '_rgt'      => 'int',
    ];

    protected $fillable = [
        'parent_id',
        'name',
        'description',
        'image',
        'status',
        '_lft',
        '_rgt'
    ];


    public function adverts()
    {
        return $this->hasMany(Advert::class)->with('priceVariants', 'images');
    }

    public function scopeParents($query, $parent_id)
    {
        return $query->where('parent_id', $parent_id);
    }

    public function scopeRoots($query)
    {
        return $query->whereNull('parent_id');
    }


}
