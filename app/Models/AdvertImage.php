<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 04 May 2019 12:14:20 +0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class AdvertImage
 *
 * @property int $id
 * @property int $advert_id
 * @property string $path
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class AdvertImage extends Eloquent
{
    protected $casts = [
        'advert_id' => 'int'
    ];

    protected $fillable = [
        'advert_id',
        'path'
    ];


    public function advert()
    {
        return $this->belongsTo(Advert::class);
    }
}
