<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 25 Feb 2019 12:35:16 +0000.
 */

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

/**
 * Class User
 *
 * @property int $id
 * @property string $user_id
 * @property string $type
 * @property string $name
 * @property string $avatar
 * @property string $phone
 * @property string $address
 * @property string $lokasyon
 * @property string $lat
 * @property string $lng
 * @property string $about
 * @property string $tc_id
 * @property string $email
 * @property \Carbon\Carbon $email_verified_at
 * @property string $password
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \Illuminate\Database\Eloquent\Collection $adverts
 * @property \Illuminate\Database\Eloquent\Collection $ratings
 *
 * @package App\Models
 */
class User extends Authenticatable
{
    use HasApiTokens, Notifiable, HasRoles;

    protected $dates = [
        'email_verified_at'
    ];

    protected $hidden = [
        'password',
        'remember_token'
    ];

    protected $fillable = [
        'user_id',
        'type',
        'name',
        'avatar',
        'phone',
        'address',
        'location',
        'lat',
        'lng',
        'about',
        'tc_id',
        'email',
        'email_verified_at',
        'password',
        'remember_token'
    ];

    public function adverts()
    {
        return $this->belongsToMany(\App\Models\Advert::class);
    }

    public function advertComments()
    {
        return $this->hasMany(\App\Models\AdvertComment::class);
    }

    public function ratings()
    {
        return $this->hasMany(\App\Models\Rating::class);
    }

    public function setting()
    {
        return $this->hasOne(UserSetting::class);
    }

    public function conversations()
    {
        // todo : buraya bakılacak
        return $this->hasMany(Conversation::class);
    }
}
