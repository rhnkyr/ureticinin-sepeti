<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 25 Feb 2019 12:35:16 +0000.
 */

namespace App\Models;

use Carbon\Carbon;
use Laravel\Scout\Searchable;
use Reliese\Database\Eloquent\Model as Eloquent;
use Spatie\Tags\HasTags;

/**
 * Class Advert
 *
 * @property int $id
 * @property string $uuid
 * @property string $user_id
 * @property string $title
 * @property int $category_id
 * @property int $price_type
 * @property string $lat
 * @property string $lng
 * @property string $area
 * @property string $description
 * @property int $point
 * @property int $attendies_count
 * @property \Carbon\Carbon $finished_at
 * @property int $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \Illuminate\Database\Eloquent\Collection $transportations
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package App\Models
 */
class Advert extends Eloquent
{

    use HasTags, Searchable;

    protected $casts = [
        'category_id'       => 'int',
        'price_type'        => 'int',
        'point'             => 'int',
        'participant_count' => 'int',
        'status'            => 'int'
    ];

    protected $dates = [
        'finished_at'
    ];

    protected $fillable = [
        'uuid',
        'user_id',
        'title',
        'category_id',
        'price_type',
        'lat',
        'lng',
        'area',
        'description',
        'point',
        'participant_count',
        'finished_at',
        'status'
    ];

    public function setFinishedAtAttribute($value)
    {
        $this->attributes['finished_at'] = Carbon::createFromFormat('d-m-Y', $value)->endOfDay();
    }


    public function toSearchableArray()
    {
        return [
            'tags' => $this->tags
        ];
    }

    public function scopeUserAdverts($query)
    {
        return $query->where('user_id', auth()->id());
    }


    public function transports()
    {
        return $this->belongsToMany(Transportation::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function comments()
    {
        return $this->hasMany(AdvertComment::class);
    }

    public function priceVariants()
    {
        return $this->hasMany(AdvertPriceVariant::class);
    }

    public function images()
    {
        return $this->hasMany(AdvertImage::class);
    }
}
