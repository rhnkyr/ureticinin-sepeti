<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 25 Feb 2019 12:35:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Conversation
 *
 * @property int $id
 * @property string $uuid
 * @property string $f_user_id
 * @property string $s_user_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Conversation extends Eloquent
{
    protected $fillable = [
        'uuid',
        'f_user_id',
        's_user_id'
    ];
}
