<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 11 May 2019 20:40:42 +0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class AdvertPriceVariant
 *
 * @property int $id
 * @property int $advert_id
 * @property int $unit
 * @property float $minimum
 * @property float $maximum
 * @property float $price
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class AdvertPriceVariant extends Eloquent
{
    protected $casts = [
        'advert_id' => 'int',
        'unit'      => 'int',
        'minimum'   => 'float',
        'maximum'   => 'float',
        'price'     => 'float'
    ];

    protected $fillable = [
        'advert_id',
        'unit',
        'minimum',
        'maximum',
        'price'
    ];

    public function advert()
    {
        return $this->belongsTo(Advert::class);
    }
}
