<?php

    namespace App\Providers;

use App\Models\User;
use App\Observers\UserObserver;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;
use Laravel\Passport\Client;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        User::observe(UserObserver::class);
        Client::creating(
            function (Client $client) {
                $client->incrementing = false;
                $client->id           = Str::uuid();
            }
            );
        Client::retrieved(
            function (Client $client) {
                $client->incrementing = false;
            }
            );
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
