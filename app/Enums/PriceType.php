<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class PriceType extends Enum
{
    const SINGLE = 1; // Sabit fiyat
    const MULTIPLE = 2; // Baremli fiyat
}
