<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BaseController extends Controller
{
    public function api($url, $method = 'GET', $params = [])
    {
        $req = \request()->create('/api/' . $url, $method, $params);
        $req->headers->set('Authorization', 'Bearer ' . $_COOKIE['access_token']);
        return app()->handle($req);
    }
}
