<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Illuminate\Support\Str;

class CategoryController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function index()
    {

        $categories = app(Category::class)
            ->with('ancestors')
            ->orderBy('parent_id')
            ->get();

        return view('admin.category.list', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $root_categories = app(Category::class)
            ->roots()
            ->get();


        return view('admin.category.create', compact('root_categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\CategoryRequest $request
     * @return void
     */
    public function store(CategoryRequest $request)
    {

        //Get file
        $file = $request->file('image');

        //Rename file
        $name = Str::slug($request->name) . '-' . time() . '.' . $file->getClientOriginalExtension();

        //Save file
        \Storage::putFileAs('categories', $request->file('image'), $name);

        //Get path of new file
        $path = \Storage::path('categories/' . $name);

        $img = \Image::make($path);

        //Crop center resize
        $img->fit(600);

        //Saveimage
        $img->save($path);

        $data = $request->all();

        $data['image'] = $name;

        $parent = app(Category::class)->find($request->parent_id);

        if ($parent) {
            app(Category::class)->create($data, $parent);
        } else {
            app(Category::class)->create($data);
        }


        return redirect(config('app.url') . '/admin/categories')->with('success', true);

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    /*public function edit($id)
    {
        $category_response        = $this->api('categories/' . $id);
        $parent_category_response = $this->api('parent-categories');
        $category                 = json_decode($category_response->getContent())->data;
        $parent_categories        = json_decode($parent_category_response->getContent());

        return view('admin.category.edit', compact('category', 'parent_categories'));
    }*/

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\CategoryRequest $request
     * @param int $id
     * @return void
     */
    /*public function update(CategoryRequest $request, $id)
    {

        if ($request->hasFile('image')) {

            //Get file
            $file = $request->file('image');

            //Rename file
            $name = Str::slug($request->name) . '-' . time() . '.' . $file->getClientOriginalExtension();

            //Save file
            \Storage::putFileAs('categories', $request->file('image'), $name);

            //Get path of new file
            $path = \Storage::path('categories/' . $name);

            $img = \Image::make($path);

            //Crop center resize
            $img->fit(600);

            //Saveimage
            $img->save($path);

            $data = $request->all();

            $data['image'] = $name;

            $category = app(Category::class)->create($data);

        } else {
            $category = app(Category::class)->create($request->except('image'));
        }


        //$data = json_decode($res->getContent());

        //if ($data->status) {
        //    return redirect(config('app.url') . '/admin/categories')->with('success', true);
        //}
    }*/

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $res = $this->api('categories/' . $id, 'DELETE');

        $data = json_decode($res->getContent());

        if ($data->status) {
            return redirect(config('app.url') . '/admin/categories')->with('success', true);
        }
    }
}
