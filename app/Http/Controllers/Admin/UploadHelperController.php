<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class UploadHelperController extends Controller
{
    public function upload(Request $request)
    {
        if ($request->hasFile('file')) {
            //Get file
            $file = $request->file('file');

            //Rename file
            $name = Str::slug($request->title) . '-' . time() . '.' . $file->getClientOriginalExtension();

            //Save file
            \Storage::putFileAs('advert-images', $request->file('file'), $name);

            //Get path of new file
            $path = \Storage::path('advert-images/' . $name);

            $img = \Image::make($path);

            //Crop center resize
            $img->fit(600);

            //Saveimage
            $img->save($path);

            return $name;
        }
    }

    public function delete(Request $request)
    {
        \Storage::delete('advert-images/' . $request->path);
    }
}
