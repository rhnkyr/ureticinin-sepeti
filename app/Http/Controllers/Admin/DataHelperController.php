<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DataHelperController extends Controller
{

    public function options(Request $request)
    {

        $categories = app(Category::class)
            ->parents($request->pid)
            ->get();


        $options = '';

        foreach ($categories as $category) {
            $options .= '<option value="' . $category->id . '">' . $category->name . '</option>';
        }

        return $options;


    }

}
