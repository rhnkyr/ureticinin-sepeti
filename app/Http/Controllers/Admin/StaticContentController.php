<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

class StaticContentController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function index()
    {

        $res             = $this->api('static-contents');
        $static_contents = json_decode($res->getContent())->data;

        return view('admin.static-content.list', compact('static_contents'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.static-content.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $res = $this->api('static-contents', 'POST', $request->all());

        dd($res);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $res  = $this->api('static-contents/' . $id);
        $user = json_decode($res->getContent())->data;

        return view('admin.static-content.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $res = $this->api('static-contents/' . $id, 'PUT', $request->all());

        dd($res);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $res = $this->api('static-contents/' . $id, 'DELETE');

        dd($res);
    }
}
