<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AdvertRequest;
use App\Models\Advert;
use App\Models\AdvertImage;
use App\Models\Category;
use App\Models\Transportation;

class AdvertController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $adverts = app(Advert::class)
            ->with('category.ancestors')
            ->orderBy('created_at', 'desc')
            ->paginate(env('PER_PAGE'));

        return view('admin.advert.list', compact('adverts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = app(Category::class)
            ->with('ancestors')
            ->whereNotNull('parent_id')
            ->orderBy('parent_id')
            ->get();


        $transports = app(Transportation::class)->orderBy('order')->get();

        return view('admin.advert.add', compact('categories', 'transports'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\AdvertRequest $request
     * @return void
     */
    public function store(AdvertRequest $request)
    {

        $res = $this->api('adverts', 'POST', $request->all());

        $data = json_decode($res->getContent());


        if ($data->status) {
            return redirect(config('app.url') . '/admin/adverts')->with('success', true);
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $advert = app(Advert::class)
            ->with('images', 'transports', 'priceVariants')
            ->findOrFail($id);

        $categories = app(Category::class)
            ->with('ancestors')
            ->whereNotNull('parent_id')
            ->orderBy('parent_id')
            ->get();

        $transports = app(Transportation::class)->orderBy('order')->get();


        return view('admin.advert.edit', compact('advert', 'categories', 'transports'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\AdvertRequest $request
     * @param int $id
     * @return void
     */
    public function update(AdvertRequest $request, $id)
    {
        $res = $this->api('adverts/' . $id, 'PUT', $request->all());

        $data = json_decode($res->getContent());

        if ($data->status) {
            return redirect(config('app.url') . '/admin/adverts')->with('success', true);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $res = $this->api('adverts/' . $id, 'DELETE');

        $data = json_decode($res->getContent());

        if ($data->status) {
            return redirect(config('app.url') . '/admin/adverts')->with('success', true);
        }
    }
}
