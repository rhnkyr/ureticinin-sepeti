<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

class TransportationController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function index()
    {

        $res        = $this->api('transports');
        $transports = json_decode($res->getContent())->data;

        return view('admin.transport.list', compact('transports'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.transport.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $res = $this->api('transports', 'POST', $request->all());

        dd($res);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $res       = $this->api('transports/' . $id);
        $transport = json_decode($res->getContent())->data;

        return view('admin.transport.edit', compact('transport'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $res = $this->api('transports/' . $id, 'PUT', $request->all());

        dd($res);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $res = $this->api('transports/' . $id, 'DELETE');

        dd($res);
    }
}
