<?php

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function index()
    {
        return view('admin.login');
    }

    public function login(Request $request)
    {
        $request->validate([
            'email'    => 'required|email',
            'password' => 'required',
        ]);

        $req = \request()->create('/api/auth/login', 'POST', $request->only('email', 'password'));
        $req->headers->set('Accept', 'application/json');
        $res          = app()->handle($req);
        $responseBody = json_decode($res->getContent());

        if (isset($responseBody->message)) {
            return redirect()->back()->withErrors(['error' => $responseBody->message]);
        }

        setcookie(
            'access_token',
            $responseBody->access_token,
            strtotime($responseBody->expires_at),
            '/'
        );

        $req = \request()->create('/api/auth/me');
        $req->headers->set('Accept', 'application/json');
        $req->headers->set('Authorization', 'Bearer ' . $responseBody->access_token);
        $res          = app()->handle($req);
        $responseBody = json_decode($res->getContent());

        $user = app(User::class)->find($responseBody->id);

        if ($user->type !== 'admin') {
            return redirect()->back()->withErrors(['error' => 'Yetkisiz kullanıcı!']);
        }

        \Auth::guard('web')->login($user, true);

        return redirect(config('app.url') . '/admin/adverts')->with('success', true);
    }

    public function getData()
    {
        $req = \request()->create('/api/categories');
        $req->headers->set('Accept', 'application/json');
        $req->headers->set('Authorization', 'Bearer ' . $_COOKIE['access_token']);
        $res          = app()->handle($req);
        $responseBody = json_decode($res->getContent())->data;
        foreach ($responseBody as $data) {
            echo $data->name;
        }
    }
}
