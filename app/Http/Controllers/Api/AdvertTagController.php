<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\AdvertTagResource;
use App\Models\Advert;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class AdvertTagController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function show($id): AnonymousResourceCollection
    {
        $tags = array_map('trim', explode('|', $id));

        $result = app(Advert::class)->withAnyTags(array_values($tags))->get();

        return AdvertTagResource::collection($result);
    }

}
