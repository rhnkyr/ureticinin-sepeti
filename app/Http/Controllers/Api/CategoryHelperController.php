<?php

namespace App\Http\Controllers\Api;

use App\Models\Category;
use App\Http\Controllers\Controller;

class CategoryHelperController extends Controller
{
    public function index()
    {
        $categories = app(Category::class)
            ->with('parent')
            ->orderBy('name')
            ->get();

        return $categories;
    }

    public function parentCategories()
    {
        $categories = app(Category::class)
            ->where('parent_id', 0)
            ->orderBy('name')
            ->get();

        return $categories;
    }
}
