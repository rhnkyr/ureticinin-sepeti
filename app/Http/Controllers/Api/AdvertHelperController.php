<?php

namespace App\Http\Controllers\Api;

use App\Models\Category;
use App\Http\Controllers\Controller;

class AdvertHelperController extends Controller
{
    public function byCategory($id)
    {

        $category = app(Category::class)->find($id);
        return $category->adverts()->orderBy('created_at')->paginate(25);
    }
}
