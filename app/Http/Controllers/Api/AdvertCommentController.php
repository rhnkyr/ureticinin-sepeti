<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Advert;
use App\Models\AdvertComment;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AdvertCommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(): Response
    {
        $data = app(AdvertComment::class)->with('advert', 'user')->paginate(10);
        return response()->json(['status' => 'success', 'data' => $data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request): Response
    {
        try {
            $advert = app(Advert::class)->where('uuid', $request->advert_id)->firstOrFail();

            $advert->comments->create(
                $request->except('advert_id', 'user_id')
                + ['user_id' => auth()->user()->id, 'advert_id' => $advert->id]
            );

        } catch (ModelNotFoundException $e) {

            return response()->json(['status' => false, 'message' => $e->getMessage()]);

        }

        return response()->json(['status' => 'success'], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id): Response
    {
        try {
            $data = app(AdvertComment::class)->findorFail($id);
            $data->delete();
        } catch (ModelNotFoundException $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()]);
        }

        return response()->json(['status' => 'success']);
    }
}
