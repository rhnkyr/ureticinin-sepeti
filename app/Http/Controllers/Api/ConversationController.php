<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ConversationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index() : JsonResponse
    {
        $conversations = \Chat::conversations()->for(auth()->user())->get();
        return response()->json(['status' => 'success', 'conversations' => $conversations]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request) : JsonResponse
    {
        $user         = app(User::class)->where('user_id', $request->to_user)->firstOrFail();
        $participants = [auth()->user()->id, $user->id];
        $conversation = \Chat::createConversation($participants)->makePrivate();

        $data = ['title' => $request->title, 'to_user_name' => $user->name];
        $conversation->update(['data' => $data]);

        return response()->json(['status' => 'success', 'conversation_id' => $conversation->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id) : JsonResponse
    {
        try {
            $conversation = \Chat::conversations()->getById($id);
            return response()->json(['status' => 'success', 'conversation' => $conversation]);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['status' => 'error', 'message' => $exception->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id) : JsonResponse
    {
        try {
            $conversation = \Chat::conversations()->getById($id);
            \Chat::conversation($conversation)->for(auth()->user())->clear();
            return response()->json(['status' => 'success']);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['status' => 'error', 'message' => $exception->getMessage()]);
        }
    }
}
