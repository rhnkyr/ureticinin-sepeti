<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserLoginRequest;
use App\Http\Requests\UserRequest;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;

class AuthController extends Controller
{
    /**
     * Create user
     * @param UserRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function signup(UserRequest $request): JsonResponse
    {
        $user = new User(
            [
                'user_id'  => Uuid::uuid4(),
                'type'     => $request->user_type,
                'name'     => $request->name,
                'email'    => $request->email,
                'phone'    => $request->phone,
                'password' => bcrypt($request->password),
            ]
        );
        $user->save();

        return response()->json(
            [
                'message' => 'Kullanıcı kaydı gerçekleştirildi!',
            ],
            201
        );
    }

    /**
     * Login user and create token
     * @param \App\Http\Requests\UserLoginRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(UserLoginRequest $request): JsonResponse
    {
        $credentials = request(['email', 'password']);
        if (!auth()->attempt($credentials)) {
            return response()->json(
                [
                    'message' => 'Yetkisiz kullanıcı!',
                ],
                401
            );
        }
        $user        = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token       = $tokenResult->token;

        $token->save();

        return response()->json(
            [
                'access_token' => $tokenResult->accessToken,
                'token_type'   => 'Bearer',
                'expires_at'   => Carbon::parse(
                    $tokenResult->token->expires_at
                )->toDateTimeString(),
            ]
        );
    }

    /**
     * Logout user (Revoke the token)
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request): JsonResponse
    {
        $request->user()->token()->revoke();

        return response()->json(
            [
                'message' => 'Sitemden çıkış gerçekleştirildi!',
            ]
        );
    }

    /**
     * Get the authenticated User
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function user(Request $request): JsonResponse
    {
        return response()->json($request->user());
    }
}
