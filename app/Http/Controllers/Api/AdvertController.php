<?php

namespace App\Http\Controllers\Api;

use App\Enums\PriceType;
use App\Http\Controllers\Controller;
use App\Models\Advert;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Ramsey\Uuid\Uuid;

class AdvertController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(): JsonResponse
    {
        $data = app(Advert::class)->all();
        return response()->json(['status' => 'success', 'data' => $data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(Request $request): JsonResponse
    {
        try {
            $tags = array_map('trim', explode(' ', trim($request->title)));


            $data = $request->except(
                'transport_ids',
                'images',
                'price_variants',
                'price_variants_simple'
            );

            $data['uuid']    = (string)Uuid::uuid4();
            $data['user_id'] = auth()->id();

            $advert = app(Advert::class)->create($data);


            $advert->attachTags(array_values($tags));


            foreach ($request->price_variants_simple as $variant) {
                if (!$variant['minimum']) {
                    break;
                }
                $advert->priceVariants()->create(
                    [
                        'unit'    => $variant['unit'],
                        'minimum' => $variant['minimum'],
                        'price'   => $variant['price'],
                    ]
                );
            }

            foreach ($request->price_variants as $variant) {
                if (!$variant['minimum']) {
                    break;
                }
                $advert->priceVariants()->create(
                    [
                        'unit'    => $variant['unit'],
                        'minimum' => $variant['minimum'],
                        'maximum' => $variant['maximum'],
                        'price'   => $variant['price'],
                    ]
                );
            }

            foreach ($request->images as $image) {
                $advert->images()->create(
                    [
                        'advert_id' => $advert->id,
                        'path'      => $image
                    ]
                );
            }

            $advert->transports()->attach(array_values($request->transport_ids));
        } catch (QueryException $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage()]);
        }

        return response()->json(['status' => true, 'message' => 'success'], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id): JsonResponse
    {
        try {
            $data = app(Advert::class)->with(['transports', 'category', 'comments', 'priceVariants','images'])->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage()]);
        }

        return response()->json(['status' => 'success', 'data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id): JsonResponse
    {
        try {
            $tags = array_map('trim', explode(' ', trim($request->title)));

            $advert = app(Advert::class)
                ->where('uuid', $id)
                ->orWhere('id', $id)
                ->firstOrFail();

            $advert->update(
                $request->except(
                    'transport_ids',
                    'images',
                    'price_variants',
                    'price_variants_simple'
                )
            );

            $advert->syncTags(array_values($tags));

            $advert->priceVariants()->delete();

            if ($request->price_type === PriceType::SINGLE) {
                foreach ($request->price_variants_simple as $variant) {
                    $advert->priceVariants()->create(
                        [
                            'unit'    => $variant['unit'],
                            'minimum' => $variant['minimum'],
                            'price'   => $variant['price'],
                        ]
                    );
                }
            } else {
                foreach ($request->price_variants as $variant) {
                    $advert->priceVariants()->create(
                        [
                            'unit'    => $variant['unit'],
                            'minimum' => $variant['minimum'],
                            'maximum' => $variant['maximum'],
                            'price'   => $variant['price'],
                        ]
                    );
                }
            }


            $advert->images()->delete();

            foreach ($request->images as $image) {
                $advert->images()->create(
                    [
                        'advert_id' => $advert->id,
                        'path'      => $image
                    ]
                );
            }

            $advert->transports()->sync(array_values($request->transport_ids));

        } catch (QueryException $e) {

            return response()->json(['status' => false, 'message' => $e->getMessage()]);

        } catch (ModelNotFoundException $e) {

            return response()->json(['status' => false, 'message' => $e->getMessage()]);

        }

        return response()->json(['status' => true, 'message' => 'Success'], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($id): JsonResponse
    {
        try {
            $advert = app(Advert::class)->findorFail($id);

            $advert->tags()->delete();
            $advert->images()->delete();
            $advert->priceVariants()->delete();
            $advert->transports()->delete();

            $advert->delete();
        } catch (ModelNotFoundException $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()]);
        }

        return response()->json(['status' => 'success']);
    }
}
