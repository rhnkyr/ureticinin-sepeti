<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Nette\NotImplementedException;

class MessageController extends Controller
{

    public function show($id)
    {
        try {
            $conversation = \Chat::conversations()->getById($id);
            $messages     = \Chat::conversation($conversation)->for(auth()->user())->getMessages(); //all messages
            \Chat::conversation($conversation)->for(auth()->user())->readAll();
            //$messages     = \Chat::conversations()->for(auth()->user())->limit(25)->page(1)->get(); //message with pagination

            return response()->json(['status' => 'success', 'messages' => $messages]);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['status' => 'error', 'message' => $exception->getMessage()]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $conversation = \Chat::conversations()->getById($request->conversation_id);

            \Chat::message($request->message)
                ->from(auth()->user()->id)
                ->to($conversation)
                ->send();

            return response()->json(['status' => 'success']);

        } catch (ModelNotFoundException $exception) {
            return response()->json(['status' => 'error', 'message' => $exception->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        throw new NotImplementedException();
//        try {
//
//            //fixme : delete not work
//            $message = \Chat::messages()->getById($id);
//            \Chat::message($message)->for(auth()->user())->delete();
//
//            return response()->json(['status' => 'success']);
//
//        } catch (ModelNotFoundException $exception) {
//            return response()->json(['status' => 'error', 'message' => $exception->getMessage()]);
//        }
    }
}
