<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdvertRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'                    => 'required|max:250',
            'category_id'              => 'required|integer',
            'price_type'               => 'required|integer',
            'area'                     => 'required|max:250',
            'description'              => 'required',
            'status'                   => 'required|integer',
            'images'                   => 'required|array|min:1',
            'images.*'                 => 'required',
            'transport_ids'            => 'required|array|min:1',
            /*'price_variants'           => 'required|array|min:1',
            'price_variants.*.unit'    => 'required',
            'price_variants.*.minimum' => 'required',
            'price_variants.*.price'   => 'required',*/
        ];
    }
}
