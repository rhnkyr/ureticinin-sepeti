<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AdvertTagResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'title'       => $this->title,
            'description' => $this->description,
            'image'       => $this->firstMedia($this),
            'price'       => 100 //$this->price->first //todo: price olayina bakilacak
        ];
    }


    private function firstMedia($advert)
    {
        $items = $advert->getMedia();
        return $items[0]->getFullUrl();
    }

}
