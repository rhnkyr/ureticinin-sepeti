<?php

namespace Tests\Feature;

use App\Models\User;
use Faker\Provider\Uuid;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
use Tests\TestCase;

class AuthTest extends TestCase
{
    use RefreshDatabase;

    /*public function setUp()
    {
        parent::setUp();
        $this->artisan('passport:install');
    }*/

    /** @test */
    public function register()
    {
        //User's data
        $data = [
            'email'                 => 'test@gmail.com',
            'name'                  => 'Test',
            'password'              => 'secret1234',
            'password_confirmation' => 'secret1234',
        ];
        //Send post request
        $response = $this->json('POST', route('api.register'), $data);
        //Assert it was successful
        $response->assertStatus(201);
        //Assert we received a token
        //$this->assertArrayHasKey('token', $response->json());
        //Delete data
        User::where('email', 'test@gmail.com')->delete();
    }

    /** @test */
    public function login()
    {
        Passport::actingAs(
            User::create([
                'user_id'  => Uuid::uuid(),
                'type'     => 'type',
                'name'     => 'test',
                'email'    => 'test@gmail.com',
                'password' => bcrypt('secret1234')
            ])
        );

        //Create user
        /*User::create([
            'user_id'  => Uuid::uuid(),
            'type'     => 'type',
            'name'     => 'test',
            'email'    => 'test@gmail.com',
            'password' => bcrypt('secret1234')
        ]);*/

        //attempt login
        $response = $this->json('POST', route('api.login'), [
            'email'    => 'test@gmail.com',
            'password' => 'secret1234',
        ]);
        //Assert it was successful and a token was received
        $response->assertStatus(200);
        $this->assertArrayHasKey('access_token', $response->json());
        //Delete the user
        User::where('email', 'test@gmail.com')->delete();
    }
}
