@extends('layouts.layout-without-navbar-flex')
@section('content')

    <div class="container-fluid flex-grow-1 container-p-y">

        <h4 class="font-weight-bold py-3 mb-4">
            Kullanıcılar
        </h4>
        <div class="row pull-right">
            <a href="{{route('categories.create',[],false)}}" class="btn btn-success"><i class="fa fa-plus-circle"></i>
                Yeni Ekle</a>
        </div>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Kullanıcı Türü</th>
                    <th>Adı Soyadı</th>
                    <th>Telefon</th>
                    <th>Email</th>
                    <th>Adres</th>
                    <th>İşlemler</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{$user->type}}</td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->phone}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->address}}</td>
                        <td>
                            <a href="{{route('users.edit',['id'=> $user->id],false)}}"
                               class="btn btn-xs btn-primary" style="margin-left: 5px"><i class="fa fa-info-circle"></i></a>
                            <form method="post" class="del" style="float: left;" action="{{route('users.destroy',['id'=>$user->id],false)}}">
                                {{ method_field('DELETE') }}
                                @csrf
                                <button type="submit" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $users->links() }}
        </div>


    </div>
    <!-- / Content -->
@endsection
@section('scripts')
    <script>
        $('.btn-danger').on('click',function(e){
            e.preventDefault();
            var form = $(this).parents('form');
            swal({
                title: "Emin misiniz?",
                text: "Bu işlemin geri dönüşü yoktur!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#CE4F4B",
                confirmButtonText: "Evet",
                cancelButtonText: "İptal",
                closeOnConfirm: false
            }, function(isConfirm){
                if (isConfirm) form.submit();
            });
        });
    </script>
@endsection
