@extends('layouts.layout-blank')

@section('styles')
    <!-- Page -->
    <link rel="stylesheet" href="{{ mix('/vendor/css/pages/authentication.css') }}">
@endsection

@section('content')
    <div class="authentication-wrapper authentication-2 ui-bg-cover ui-bg-overlay-container px-4"
         style="background-image: url('/16.jpg');">
        <div class="ui-bg-overlay bg-dark opacity-25"></div>

        <div class="authentication-inner py-5">

            <div class="card">
                <div class="p-4 p-sm-5">

                    <h5 class="text-center text-muted font-weight-normal mb-4">Yönetim Paneli Giriş</h5>

                    <!-- Form -->
                    <form method="post" action="{{route('login')}}">
                        @if($errors->any())
                            <div class="alert alert-dark-danger text-center">
                                {{$errors->first()}}
                            </div>
                        @endif
                        @csrf
                        <div class="form-group">
                            <label class="form-label">Email</label>
                            <input type="text" name="email"
                                   class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}">
                            @if ($errors->has('email'))
                                <span class="invalid-feedback">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="form-label d-flex justify-content-between align-items-end">
                                <div>Şifre</div>
                            </label>
                            <input type="password" name="password"
                                   class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}">
                            @if ($errors->has('password'))
                                <span class="invalid-feedback">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="d-flex justify-content-between align-items-center m-0">
                            <button type="submit" class="btn btn-primary">Giriş</button>
                        </div>
                    </form>
                    <!-- / Form -->

                </div>
            </div>

        </div>
    </div>
@endsection
