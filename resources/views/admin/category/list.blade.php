@extends('layouts.layout-without-navbar-flex')
@section('content')

    <div class="container-fluid flex-grow-1 container-p-y">

        <h4 class="font-weight-bold py-3 mb-4">
            Kategoriler
        </h4>
        <div class="row pull-right">
            <a href="{{route('categories.create',[],false)}}" class="btn btn-success"><i class="fa fa-plus-circle"></i>
                Yeni Ekle</a>
        </div>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Başlık</th>
                    <th>Üst Kategori</th>
                    <th>Açıklama</th>
                    <th>İşlemler</th>
                </tr>
                </thead>
                <tbody>
                @foreach($categories as $category)
                    <tr>
                        <td>{{$category->id}}</td>
                        <td>{{$category->name}}</td>
                        <td>@if($category->ancestors->count()){{$category->ancestors->pluck('name')->last()}}@else{{ '-' }}@endif</td>
                        <td>{{$category->description}}</td>
                        <td>
                            <form method="post" class="del" style="float: left;" action="{{route('categories.destroy',['id'=>$category->id],false)}}">
                                {{ method_field('DELETE') }}
                                @csrf
                                <button type="submit" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>


    </div>
    <!-- / Content -->
@endsection
@section('scripts')
<script>
    $('.btn-danger').on('click',function(e){
        e.preventDefault();
        var form = $(this).parents('form');
        swal({
            title: "Emin misiniz?",
            text: "Bu işlemin geri dönüşü yoktur!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#CE4F4B",
            confirmButtonText: "Evet",
            cancelButtonText: "İptal",
            closeOnConfirm: false
        }, function(isConfirm){
            if (isConfirm) form.submit();
        });
    });
</script>
@endsection
