@extends('layouts.layout-without-navbar-flex')
@section('content')

    <div class="container-fluid flex-grow-1 container-p-y">

        <h4 class="font-weight-bold py-3 mb-4">
            Yeni Kategori
        </h4>

        <div class="card mb-4">
            <div class="card-body">
                <form method="post" enctype="multipart/form-data" action="{{route('categories.store',[],false)}}">
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label class="form-label">Ana Kategori</label>
                            <select id="parent"
                                    class="form-control">
                                <option value="0">Yok</option>
                                @foreach($root_categories as $rc)
                                    <option value="{{$rc->id}}">{{$rc->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-4">
                            <div id="level1-div" class="form-group">
                                <label class="form-label">1. Alt Kategori</label>
                                <select id="level1" class="form-control">
                                    <option value="0">Yok</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div id="level2-div" class="form-group">
                                <label class="form-label">2. Alt Kategori</label>
                                <select id="level2" class="form-control">
                                    <option value="0">Yok</option>
                                </select>
                            </div>
                        </div>
                        <input type="hidden" id="parent_id" name="parent_id" value="0">
                    </div>
                    <div class="form-group">
                        <label class="form-label">Adı</label>
                        <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                               name="name">
                        @if ($errors->has('name'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="form-label">Açıklama</label>
                        <input type="text" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}"
                               name="description">
                        @if ($errors->has('description'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="form-label">Görsel</label>
                        <input type="file" class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }}"
                               name="image">
                        @if ($errors->has('image'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('image') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label class="form-label">Durum</label>
                        <select class="form-control{{ $errors->has('status') ? ' is-invalid' : '' }}" name="status">
                            <option value="1">Aktif</option>
                            <option value="0">Pasif</option>
                        </select>
                        @if ($errors->has('status'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('status') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="row pull-right">
                        <button type="submit" class="btn btn-primary">Kaydet</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- / Content -->
@endsection
@section('scripts')
    <script>
        $(function () {

            const token = '{{csrf_token()}}';

            $('#level1-div').hide();
            $('#level2-div').hide();

            $('#parent').on('change', function () {
                let val = $(this).val();
                if (parseInt(val) !== 0) {
                    $.post('{{route('options',[],false)}}', {_token: token, pid: val}, function (res) {
                        $('#level1').children('option:not(:first)').remove();
                        $('#level1').append(res);
                        $('#level1-div').show();
                    });
                } else {
                    $('#level1').children('option:not(:first)').remove();
                    $('#level2').children('option:not(:first)').remove();
                    $('#level1-div').hide();
                    $('#level2-div').hide();
                }

                parent_selector(val);
            });
            $('#level1').on('change', function () {
                let val = $(this).val();
                if (parseInt(val) !== 0) {
                    $.post('{{route('options',[],false)}}', {_token: token, pid: val}, function (res) {
                        $('#level2').children('option:not(:first)').remove();
                        $('#level2').append(res);
                        $('#level2-div').show();
                    });
                } else {
                    $('#level2').children('option:not(:first)').remove();
                    $('#level2-div').hide();
                }

                parseInt(val) === 0
                    ?
                    parent_selector($('#parent').val())
                    :
                    parent_selector(val);

            });

            $('#level2').on('change', function () {

                let val = $(this).val();

                parseInt(val) === 0
                    ?
                    parent_selector($('#level1').val())
                    :
                    parent_selector(val);

            });
        });

        function parent_selector(id) {
            $('#parent_id').val(id);
        }
    </script>
@endsection


