@extends('layouts.layout-without-navbar-flex')
@section('content')

    <div class="container-fluid flex-grow-1 container-p-y">

        <h4 class="font-weight-bold py-3 mb-4">
            Kategori Güncelle
        </h4>

        <div class="card mb-4">
            <div class="card-body">
                <form method="post" enctype="multipart/form-data"
                      action="{{route('categories.update',['id'=>$category->id],false)}}">
                    {{ method_field('PUT') }}
                    @csrf
                    <div class="form-group">
                        <label class="form-label">Ana Kategori</label>
                        <select class="form-control{{ $errors->has('parent_id') ? ' is-invalid' : '' }}" name="parent_id">
                            <option value="0">Yok</option>
                            @foreach($parent_categories as $pc)
                                <option value="{{$pc->id}}"
                                        @if($pc->id === $category->parent_id) selected @endif>{{$pc->name}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('parent_id'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('parent_id') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="form-label">Adı</label>
                        <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{$category->name}}">
                        @if ($errors->has('name'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="form-label">Açıklama</label>
                        <input type="text" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" value="{{$category->description}}">
                        @if ($errors->has('description'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="form-label">Görsel</label>
                        <br>
                        <img src="/storage/categories/{{$category->image}}" style="width: 300px">
                        <input type="file" name="image" class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }}">
                        @if ($errors->has('image'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('image') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label class="form-label">Durum</label>
                        <select class="form-control{{ $errors->has('status') ? ' is-invalid' : '' }}" name="status">
                            <option value="1" @if($category->status === 1) selected @endif>Aktif</option>
                            <option value="0" @if($category->status === 0) selected @endif>Pasif</option>
                        </select>
                        @if ($errors->has('status'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('status') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="row pull-right">
                        <button type="submit" class="btn btn-primary">Kaydet</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- / Content -->
@endsection


