@extends('layouts.layout-without-navbar-flex')
@section('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-lite.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ mix('/vendor/libs/dropzone/dropzone.css') }}">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/blueimp-gallery/2.33.0/css/blueimp-gallery.min.css">
    <link rel="stylesheet" href="{{ mix('/vendor/libs/blueimp-gallery/gallery-indicator.css') }}">
@endsection

@section('content')

    <div class="container-fluid flex-grow-1 container-p-y">

        <h4 class="font-weight-bold py-3 mb-4">
            İlan Güncelle
        </h4>

        <div class="card mb-4">
            <div class="card-body">
                <form id="form" method="post" enctype="multipart/form-data"
                      action="{{route('adverts.update',['id'=>$advert->id],false)}}">
                    {{ method_field('PUT') }}
                    @csrf
                    <div class="form-group">
                        <label class="form-label">Kategori</label>
                        <select class="form-control{{ $errors->has('category_id') ? ' is-invalid' : '' }}"
                                name="category_id">
                            @foreach($categories as $c)
                                <option value="{{$c->id}}"
                                        @if($advert->category_id === $c->id) selected @endif>{{implode(' -> ', $c->ancestors->pluck('name')->toArray()).' -> '.$c->name}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('category_id'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('category_id') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="form-label">Başlık</label>
                        <input type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}"
                               name="title" value="{{$advert->title}}">
                        @if ($errors->has('title'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="form-label">Açıklama</label>
                        <textarea id="summernote"
                                  class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}"
                                  style="height: 300px"
                                  name="description">{{$advert->description}}</textarea>
                        @if ($errors->has('description'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="form-label">Bölge</label>
                        <input type="text" class="form-control{{ $errors->has('area') ? ' is-invalid' : '' }}"
                               name="area" value="{{$advert->area}}">
                        @if ($errors->has('area'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('area') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="form-label">Konum Enlem</label>
                        <input type="text" class="form-control{{ $errors->has('lat') ? ' is-invalid' : '' }}" name="lat"
                               value="{{$advert->lat}}">
                        @if ($errors->has('lat'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('lat') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="form-label">Konum Boylam</label>
                        <input type="text" class="form-control{{ $errors->has('lng') ? ' is-invalid' : '' }}" name="lng"
                               value="{{$advert->lng}}">
                        @if ($errors->has('lng'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('lng') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="form-label">Nakliye Türü</label>
                        <br>
                        @foreach($transports as $transport)
                            <label class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" value="{{$transport->id}}"
                                       name="transport_ids[]"
                                       @if(in_array($transport->id, $advert->transports->pluck('id')->toArray())) checked @endif>
                                <span class="form-check-label">
                                  {{$transport->name}}
                                </span>
                            </label>
                        @endforeach
                        @if ($errors->has('transport_ids'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('transport_ids') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="form-label">Puan</label>
                        <input type="text" class="form-control{{ $errors->has('point') ? ' is-invalid' : '' }}"
                               name="point" value="{{$advert->point}}">
                        @if ($errors->has('point'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('point') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="form-label">Fiyat Türü</label>
                        <select id="price_type"
                                class="form-control{{ $errors->has('price_type') ? ' is-invalid' : '' }}"
                                name="price_type">
                            <option value="1" @if($advert->price_type === 1) selected @endif>Sabit</option>
                            <option value="2" @if($advert->price_type === 2) selected @endif>Baremli</option>
                        </select>
                        @if ($errors->has('price_type'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('price_type') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group" id="simple" style="@if($advert->price_type === 2) display: none @endif">
                        <label class="form-label">Fiyat</label>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <select class="form-control" name="price_variants_simple[0][unit]">
                                    <option value="1" @if($advert->priceVariants[0]->unit === 1) selected @endif>Kilo
                                    </option>
                                    <option value="2" @if($advert->priceVariants[0]->unit === 2) selected @endif>Ton
                                    </option>
                                    <option value="3" @if($advert->priceVariants[0]->unit === 3) selected @endif>Litre
                                    </option>
                                    <option value="4" @if($advert->priceVariants[0]->unit === 4) selected @endif>Adet
                                    </option>
                                    <option value="5" @if($advert->priceVariants[0]->unit === 5) selected @endif>Dönüm
                                    </option>
                                    <option value="6" @if($advert->priceVariants[0]->unit === 6) selected @endif>Metre
                                    </option>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <input class="form-control allownumericwithdecimal" type="text" placeholder="Oran"
                                       name="price_variants_simple[0][minimum]"
                                       value="{{$advert->priceVariants[0]->minimum}}"/>
                            </div>
                            <div class="col-md-2">
                                <input class="form-control pv" type="text" placeholder="Tutar"
                                       name="price_variants_simple[0][price]"
                                       value="{{$advert->priceVariants[0]->price}}"/>
                            </div>
                        </div>

                        @if ($errors->has('price_variants.*'))
                            <span class="invalid-feedback" style="display: block">
                                    <strong>Fiyat için tüm alanlar gereklidir</strong>
                                </span>
                        @endif
                    </div>

                    <div class="form-group repeater" id="multiple"
                         style="@if(old('price_type') === '1') display: none @endif">
                        <label class="form-label">Fiyat Barem</label>
                        <div data-repeater-list="price_variants">
                            @foreach($advert->priceVariants as $pv)
                                <div data-repeater-item>
                                    <div class="form-group row">
                                        <div class="col-md-2">
                                            <select class="form-control" name="unit">
                                                <option value="1" @if($pv->unit === 1) selected @endif>Kilo</option>
                                                <option value="2" @if($pv->unit === 2) selected @endif>Ton</option>
                                                <option value="3" @if($pv->unit === 3) selected @endif>Litre</option>
                                                <option value="4" @if($pv->unit === 4) selected @endif>Adet</option>
                                                <option value="5" @if($pv->unit === 5) selected @endif>Dönüm</option>
                                                <option value="6" @if($pv->unit === 6) selected @endif>Metre</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <input class="form-control allownumericwithdecimal" type="text"
                                                   placeholder="Minimum"
                                                   name="price_variants[{{$loop->index}}][minimum]"
                                                   value="{{$pv->minimum}}"/>
                                        </div>
                                        <div class="col-md-2">
                                            <input class="form-control allownumericwithdecimal" type="text"
                                                   placeholder="Maximum"
                                                   name="price_variants[{{$loop->index}}][maximum]"
                                                   value="{{$pv->maximum}}"/>
                                        </div>
                                        <div class="col-md-2">
                                            <input class="form-control pv" type="text" placeholder="Tutar"
                                                   name="price_variants[{{$loop->index}}][price]"
                                                   value="{{$pv->price}}"/>
                                        </div>
                                        <input class="btn btn-danger" data-repeater-delete type="button" value="Sil"/>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <input class="btn btn-primary" data-repeater-create type="button" value="Yeni Barem Ekle"/>
                        @if ($errors->has('price_variants.*'))
                            <span class="invalid-feedback" style="display: block">
                                    <strong>Fiyat Barem için tüm alanlar gereklidir</strong>
                                </span>
                        @endif
                    </div>


                    <div class="form-group">
                        <label class="form-label">İlan Bitiş Tarihi</label>
                        <input type="text" class="form-control{{ $errors->has('finished_at') ? ' is-invalid' : '' }}"
                               name="finished_at" id="finished_at"
                               value="{{$advert->finished_at->format('d-m-Y')}}">
                        @if ($errors->has('finished_at'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('finished_at') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label class="form-label">Durum</label>
                        <select class="form-control{{ $errors->has('status') ? ' is-invalid' : '' }}" name="status">
                            <option value="1" @if($advert->status === 1) selected @endif>Aktif</option>
                            <option value="0" @if($advert->status === 0) selected @endif>Pasif</option>
                        </select>
                        @if ($errors->has('status'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('status') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <!-- Lightbox template -->
                        <div id="gallery-lightbox" class="blueimp-gallery blueimp-gallery-controls">
                            <div class="slides"></div>
                            <h3 class="title"></h3>
                            <a class="prev">‹</a>
                            <a class="next">›</a>
                            <a class="close">×</a>
                            <a class="play-pause"></a>
                            <ol class="indicator"></ol>
                        </div>

                        <div id="gallery-thumbnails" class="row form-row">
                            <!-- Add this element to properly relayout grid -->
                            <div class="gallery-sizer col-sm-6 col-md-6 col-xl-3 position-absolute"></div>

                            @foreach($advert->images as $image)
                                <div class="gallery-thumbnail col-sm-6 col-md-6 col-xl-3 mb-2">
                                    <a href="/storage/advert-images/{{$image->path}}"
                                       class="img-thumbnail img-thumbnail-zoom-in">
                                        <span class="img-thumbnail-overlay bg-dark opacity-25"></span>
                                        <span class="img-thumbnail-content display-4 text-white">
                    <i class="ion ion-ios-search"></i>
                  </span>
                                        <img src="/storage/advert-images/{{$image->path}}" class="img-fluid">
                                    </a>
                                </div>
                            @endforeach

                        </div>
                    </div>

                    <div class="form-group" id="drop">
                        <div id="dropzone" class="dropzone">
                            <div class="dz-default dz-message">İlan görseli yüklemek için tıklayınız!</div>
                        </div>
                        @foreach($advert->images as $image)
                            <input name="images[]" type="hidden" value="{{$image->path}}"/>
                        @endforeach
                    </div>

                    <div class="row pull-right">
                        <button type="submit" class="btn btn-primary">Kaydet</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- / Content -->
@endsection
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-lite.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/lang/summernote-tr-TR.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.repeater/1.2.1/jquery.repeater.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
    <script src="{{ mix('/vendor/libs/dropzone/dropzone.js') }}"></script>
    <script src="{{ mix('/vendor/libs/blueimp-gallery/gallery.js') }}"></script>
    <script src="{{ mix('/vendor/libs/blueimp-gallery/gallery-fullscreen.js') }}"></script>
    <script src="{{ mix('/vendor/libs/blueimp-gallery/gallery-indicator.js') }}"></script>
    <script>
        $(function () {

            if ($('#price_type').val() === '1') {
                $('#multiple').hide();
            }

            $(document).on("keypress keyup blur", '.allownumericwithdecimal', function (event) {
                $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
                if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                    event.preventDefault();
                }
            });

            $('.pv').mask('000,000,000,000,000.00', {reverse: true});
            $('#finished_at').mask('00-00-0000');

            $('#summernote').summernote({
                toolbar: [
                    // [groupName, [list of button]]
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['fontsize', ['fontsize']],
                    ['para', ['ul', 'ol', 'paragraph']],
                ],
                lang: 'tr-TR',
                tabsize: 2,
                height: 250
            });

            $('#dropzone').dropzone({
                parallelUploads: 2,
                maxFilesize: 50000,
                acceptedFiles: 'image/*',
                addRemoveLinks: true,
                url: "{{route('upload-image',[],false)}}",
                headers: {
                    'x-csrf-token': '{{csrf_token()}}',
                },
                sending: function (file, xhr, formData) {
                    // Will send the filesize along with the file as POST data.
                    formData.append("title", $('#title').val());
                },
                success: function (file, response) {
                    let imgName = response;
                    file.previewElement.classList.add("dz-success");
                    $(file.previewTemplate).append('<span class="server_file" style="display: none">' + imgName + '</span>');
                    $('#drop').append('<input name="images[]" id="' + (Math.floor(Math.random() * (9999 - 1000 + 1)) + 1000) + '" type="hidden" value="' + imgName + '"/>');
                },
                removedfile: function (file) {
                    var server_file = $(file.previewTemplate).children('.server_file').text();
                    $.post("{{route('delete-image',[],false)}}", {
                        "_token": "{{ csrf_token() }}",
                        path: server_file
                    }, function (res) {
                    });
                    $(document).find(file.previewElement).remove();
                },
                error: function (file, response) {
                    file.previewElement.classList.add("dz-error");
                }
            });

            $('#price_type').on('change', function () {
                let selected = $(this).val();
                if (parseInt(selected) === 1) {
                    $('#simple').show();
                    $('#multiple').hide();
                } else {
                    $('#simple').hide();
                    $('#multiple').show();
                }
            });

            $('.repeater').repeater({
                defaultValues: {
                    'unit': '1'
                },
                show: function () {
                    $(this).slideDown();
                    $('.pv').mask('000,000,000,000,000.00', {reverse: true});
                },
                hide: function (deleteElement) {
                    swal({
                        title: "Emin misiniz?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#CE4F4B",
                        confirmButtonText: "Evet",
                        cancelButtonText: "İptal",
                        closeOnConfirm: false
                    }, function (isConfirm) {
                        if (isConfirm) $(this).slideUp(deleteElement);
                        swal.close()
                    });
                },
                isFirstItemUndeletable: true
            })

            $('#gallery-thumbnails').on('click', '.gallery-thumbnail > .img-thumbnail', function (e) {
                e.preventDefault();

                // Select only visible thumbnails
                var links = $('#gallery-thumbnails').find('.gallery-thumbnail:not(.d-none) > .img-thumbnail');

                window.blueimpGallery(links, {
                    container: '#gallery-lightbox',
                    carousel: true,
                    hidePageScrollbars: true,
                    disableScroll: true,
                    index: this
                });
            });
        });
    </script>
@endsection



