@extends('layouts.layout-without-navbar-flex')
@section('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-lite.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ mix('/vendor/libs/dropzone/dropzone.css') }}">
@endsection

@section('content')

    <div class="container-fluid flex-grow-1 container-p-y">

        <h4 class="font-weight-bold py-3 mb-4">
            İlan Ekle
        </h4>

        <div class="card mb-4">
            <div class="card-body">
                <form id="form" method="post" action="{{route('adverts.store',[],false)}}">
                    @csrf
                    <input type="hidden" name="uuid" value="{{auth()->id()}}">
                    <div class="form-group">
                        <label class="form-label">Kategori</label>
                        <select class="form-control{{ $errors->has('category_id') ? ' is-invalid' : '' }}"
                                name="category_id">
                            @foreach($categories as $c)
                                <option value="{{$c->id}}"
                                        @if(old('category_id') === $c->id) selected @endif>{{implode(' -> ', $c->ancestors->pluck('name')->toArray()).' -> '.$c->name}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('category_id'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('category_id') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="form-label">Başlık</label>
                        <input id="title" type="text"
                               class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}"
                               name="title" value="{{old('title')}}">
                        @if ($errors->has('title'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="form-label">Açıklama</label>
                        <textarea id="summernote"
                                  class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}"
                                  style="height: 300px"
                                  name="description">{{old('description')}}</textarea>
                        @if ($errors->has('description'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="form-label">Bölge</label>
                        <input type="text" class="form-control{{ $errors->has('area') ? ' is-invalid' : '' }}"
                               name="area" value="{{old('area')}}">
                        @if ($errors->has('area'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('area') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="form-label">Konum Enlem</label>
                        <input type="text" class="form-control{{ $errors->has('lat') ? ' is-invalid' : '' }}"
                               name="lat" value="{{old('lat')}}">
                        @if ($errors->has('lat'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('lat') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="form-label">Konum Boylam</label>
                        <input type="text" class="form-control{{ $errors->has('lng') ? ' is-invalid' : '' }}"
                               name="lng" value="{{old('lng')}}">
                        @if ($errors->has('lng'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('lng') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="form-label">Nakliye Türü</label>
                        <br>
                        @foreach($transports as $transport)
                            <label class="form-check form-check-inline">
                                <input class="form-check-input {{ $errors->has('transport_ids') ? ' is-invalid' : '' }}"
                                       type="checkbox" value="{{$transport->id}}"
                                       name="transport_ids[]"
                                       @if(old('transport_ids') && in_array($transport->id,old('transport_ids'))) checked @endif>
                                <span class="form-check-label">
                                  {{$transport->name}}
                                </span>
                            </label>
                        @endforeach
                        @if ($errors->has('transport_ids'))
                            <span class="invalid-feedback" style="display: block">
                                <strong>{{ $errors->first('transport_ids') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="form-label">Puan</label>
                        <input type="text" class="form-control" name="point" value="{{old('point')}}">
                    </div>
                    <div class="form-group">
                        <label class="form-label">Fiyat Türü</label>
                        <select id="price_type"
                                class="form-control{{ $errors->has('price_type') ? ' is-invalid' : '' }}"
                                name="price_type">
                            <option value="1" @if(old('price_type') === '1') selected @endif>Sabit</option>
                            <option value="2" @if(old('price_type') === '2') selected @endif>Baremli</option>
                        </select>
                        @if ($errors->has('price_type'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('price_type') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group" id="simple" style="@if(old('price_type') === '2') display: none @endif">
                        <label class="form-label">Fiyat</label>
                        <div class="form-group row">
                            <div class="col-md-2">
                                <select class="form-control" name="price_variants_simple[0][unit]">
                                    <option value="1">Kilo</option>
                                    <option value="2">Ton</option>
                                    <option value="3">Litre</option>
                                    <option value="4">Adet</option>
                                    <option value="5">Dönüm</option>
                                    <option value="6">Metre</option>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <input class="form-control allownumericwithdecimal" type="text" placeholder="Oran"
                                       name="price_variants_simple[0][minimum]"/>
                            </div>
                            <div class="col-md-2">
                                <input class="form-control pv" type="text" placeholder="Tutar"
                                       name="price_variants_simple[0][price]"/>
                            </div>
                        </div>

                        @if ($errors->has('price_variants.*'))
                            <span class="invalid-feedback" style="display: block">
                                    <strong>Fiyat için tüm alanlar gereklidir</strong>
                                </span>
                        @endif
                    </div>

                    <div class="form-group repeater" id="multiple"
                         style="@if(old('price_type') === '1') display: none @endif">
                        <label class="form-label">Fiyat Barem</label>
                        <div data-repeater-list="price_variants">
                            <div data-repeater-item>
                                <div class="form-group row">
                                    <div class="col-md-2">
                                        <select class="form-control" name="unit">
                                            <option value="1">Kilo</option>
                                            <option value="2">Ton</option>
                                            <option value="3">Litre</option>
                                            <option value="4">Adet</option>
                                            <option value="5">Dönüm</option>
                                            <option value="6">Metre</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <input class="form-control allownumericwithdecimal" type="text"
                                               placeholder="Minimum" name="minimum"/>
                                    </div>
                                    <div class="col-md-2">
                                        <input class="form-control allownumericwithdecimal" type="text"
                                               placeholder="Maximum" name="maximum"/>
                                    </div>
                                    <div class="col-md-2">
                                        <input class="form-control pv" type="text" placeholder="Tutar" name="price"/>
                                    </div>
                                    <input class="btn btn-danger" data-repeater-delete type="button" value="Sil"/>
                                </div>
                            </div>
                        </div>
                        <input class="btn btn-primary" data-repeater-create type="button" value="Yeni Barem Ekle"/>
                        @if ($errors->has('price_variants.*'))
                            <span class="invalid-feedback" style="display: block">
                                    <strong>Fiyat Barem için tüm alanlar gereklidir</strong>
                                </span>
                        @endif
                    </div>


                    <div class="form-group">
                        <label class="form-label">İlan Bitiş Tarihi</label>
                        <input type="text" class="form-control{{ $errors->has('finished_at') ? ' is-invalid' : '' }}"
                               name="finished_at" id="finished_at" value="{{old('finished_at')}}">
                        @if ($errors->has('finished_at'))
                            <span class="invalid-feedback">
                                    <strong>{{ $errors->first('finished_at') }}</strong>
                                </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label class="form-label">Durum</label>
                        <select class="form-control{{ $errors->has('status') ? ' is-invalid' : '' }}" name="status">
                            <option value="1" @if(old('status') === '1') selected @endif>Aktif</option>
                            <option value="0" @if(old('status') === '0') selected @endif>Pasif</option>
                        </select>
                        @if ($errors->has('status'))
                            <span class="invalid-feedback">
                                    <strong>{{ $errors->first('status') }}</strong>
                                </span>
                        @endif
                    </div>

                    <div class="form-group" id="drop">
                        <div id="dropzone" class="dropzone">
                            <div class="dz-default dz-message">İlan görseli yüklemek için tıklayınız!</div>
                        </div>
                        @if ($errors->has('images'))
                            <span class="invalid-feedback" style="display: block">
                                    <strong>{{ $errors->first('images') }}</strong>
                                </span>
                        @endif
                    </div>

                    <div class="row pull-right">
                        <button type="submit" class="btn btn-primary">Kaydet</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- / Content -->
@endsection
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-lite.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/lang/summernote-tr-TR.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.repeater/1.2.1/jquery.repeater.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>

    <script src="{{ mix('/vendor/libs/dropzone/dropzone.js') }}"></script>
    <script>
        $(function () {

            if ($('#price_type').val() === '1') {
                $('#multiple').hide();
            }

            $(document).on("keypress keyup blur", '.allownumericwithdecimal', function (event) {
                $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
                if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                    event.preventDefault();
                }
            });

            $('.pv').mask('000,000,000,000,000.00', {reverse: true});
            $('#finished_at').mask('00-00-0000');

            $('#summernote').summernote({
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['fontsize', ['fontsize']],
                    ['para', ['ul', 'ol', 'paragraph']],
                ],
                lang: 'tr-TR',
                tabsize: 2,
                height: 250
            });

            $('#dropzone').dropzone({
                parallelUploads: 2,
                maxFilesize: 50000,
                acceptedFiles: 'image/*',
                addRemoveLinks: true,
                url: "{{route('upload-image',[],false)}}",
                headers: {
                    'x-csrf-token': '{{csrf_token()}}',
                },
                sending: function (file, xhr, formData) {
                    // Will send the filesize along with the file as POST data.
                    formData.append("title", $('#title').val());
                },
                success: function (file, response) {
                    let imgName = response;
                    file.previewElement.classList.add("dz-success");
                    $(file.previewTemplate).append('<span class="server_file" style="display: none">' + imgName + '</span>');
                    $('#drop').append('<input name="images[]" id="' + (Math.floor(Math.random() * (9999 - 1000 + 1)) + 1000) + '" type="hidden" value="' + imgName + '"/>');
                },
                removedfile: function (file) {
                    var server_file = $(file.previewTemplate).children('.server_file').text();
                    $.post("{{route('delete-image',[],false)}}", {
                        "_token": "{{ csrf_token() }}",
                        path: server_file
                    }, function (res) {
                    });
                    $(document).find(file.previewElement).remove();
                },
                error: function (file, response) {
                    file.previewElement.classList.add("dz-error");
                }
            });

            $('#price_type').on('change', function () {
                let selected = $(this).val();
                if (parseInt(selected) === 1) {
                    $('#simple').show();
                    $('#multiple').hide();
                } else {
                    $('#simple').hide();
                    $('#multiple').show();
                }
            });

            $('.repeater').repeater({
                defaultValues: {
                    'unit': '1'
                },
                show: function () {
                    $(this).slideDown();
                    $('.pv').mask('000,000,000,000,000.00', {reverse: true});
                },
                hide: function (deleteElement) {
                    swal({
                        title: "Emin misiniz?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#CE4F4B",
                        confirmButtonText: "Evet",
                        cancelButtonText: "İptal",
                        closeOnConfirm: false
                    }, function (isConfirm) {
                        if (isConfirm) $(this).slideUp(deleteElement);
                        swal.close()
                    });
                },
                isFirstItemUndeletable: true
            })
        });


    </script>
@endsection



