<?php $routeName = Route::currentRouteName(); ?>
<!-- Layout sidenav -->
<div id="layout-sidenav" class="{{ isset($layout_sidenav_horizontal) ? 'layout-sidenav-horizontal sidenav-horizontal container-p-x flex-grow-0' : 'layout-sidenav sidenav-vertical' }} sidenav bg-sidenav-theme">
    @empty($layout_sidenav_horizontal)

    <div class="sidenav-divider mt-0"></div>
    @endempty

    <!-- Links -->
    <ul class="sidenav-inner{{ empty($layout_sidenav_horizontal) ? ' py-1' : '' }}">

        <!-- Dashboards -->
        <li class="sidenav-item{{ strpos($routeName, 'admin.') === 0 ? ' active' : '' }}">
            <a href="javascript:void(0)" class="sidenav-link"><div>Yönetim Paneli</div></a>
        </li>
        <li class="sidenav-item{{ strpos($routeName, 'dashboard.') === 0 ? ' active' : '' }}">
            <a href="javascript:void(0)" class="sidenav-link"><i class="sidenav-icon fa fa-home"></i><div class="text-center">Güncel</div></a>
        </li>

        <li class="sidenav-item{{ strpos($routeName, 'adverts.') === 0 ? ' active' : '' }}">
            <a href="{{route('adverts.index',[],false)}}" class="sidenav-link"><i class="sidenav-icon fa fa-plus-circle"></i><div>İlanlar</div></a>
        </li>

        <!--<li class="sidenav-item{{ strpos($routeName, 'advert-comments.') === 0 ? ' active' : '' }}">
            <a href="{{route('advert-comments.index',[],false)}}" class="sidenav-link"><i class="sidenav-icon fa fa-exclamation-circle"></i><div>İlan Yorumları</div></a>
        </li>-->

        <li class="sidenav-item{{ strpos($routeName, 'categories.') === 0 ? ' active' : '' }}">
            <a href="{{route('categories.index',[],false)}}" class="sidenav-link"><i class="sidenav-icon fa fa-list-ul"></i><div>Kategoriler</div></a>
        </li>

        <!--<li class="sidenav-item{{ strpos($routeName, 'users.') === 0 ? ' active' : '' }}">
            <a href="{{route('users.index',[],false)}}" class="sidenav-link"><i class="sidenav-icon fa fa-users"></i><div>Kullanıcılar</div></a>
        </li>

        <li class="sidenav-item{{ strpos($routeName, 'faqs.') === 0 ? ' active' : '' }}">
            <a href="{{route('faqs.index',[],false)}}" class="sidenav-link"><i class="sidenav-icon fa fa-question-circle"></i><div>SSS</div></a>
        </li>


        <li class="sidenav-item{{ strpos($routeName, 'prices.') === 0 ? ' active' : '' }}">
            <a href="{{route('prices.index',[],false)}}" class="sidenav-link"><i class="sidenav-icon fa fa-dollar-sign"></i><div>Fiyatlar</div></a>
        </li>

        <li class="sidenav-item{{ strpos($routeName, 'transports.') === 0 ? ' active' : '' }}">
            <a href="{{route('transports.index',[],false)}}" class="sidenav-link"><i class="sidenav-icon fa fa-tractor"></i><div>Naklite Tipleri</div></a>
        </li>

        <li class="sidenav-item{{ strpos($routeName, 'static-contents.') === 0 ? ' active' : '' }}">
            <a href="{{route('static-contents.index',[],false)}}" class="sidenav-link"><i class="sidenav-icon fa fa-pagelines"></i><div>Sabit İçerik</div></a>
        </li>-->


    </ul>
</div>
<!-- / Layout sidenav -->
