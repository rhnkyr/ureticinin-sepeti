<!DOCTYPE html>

<html lang="{{app()->getLocale()}}" class="default-style">
<head>
    <title>Üreticinin Sepeti Yönetim Paneli</title>

    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="IE=edge,chrome=1">
    <meta name="description" content="">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
    <link rel="icon" type="image/x-icon" href="favicon.ico">

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900"
          rel="stylesheet">

    <!-- Icon fonts -->
    <link rel="stylesheet" href="{{ mix('/vendor/fonts/fontawesome.css') }}">
    <link rel="stylesheet" href="{{ mix('/vendor/fonts/ionicons.css') }}">
    <link rel="stylesheet" href="{{ mix('/vendor/fonts/linearicons.css') }}">
    <link rel="stylesheet" href="{{ mix('/vendor/fonts/open-iconic.css') }}">
    <link rel="stylesheet" href="{{ mix('/vendor/fonts/pe-icon-7-stroke.css') }}">

    <!-- Core stylesheets -->
    <link rel="stylesheet" href="{{ mix('/vendor/css/rtl/bootstrap.css') }}" class="theme-settings-bootstrap-css">
    <link rel="stylesheet" href="{{ mix('/vendor/css/rtl/appwork.css') }}" class="theme-settings-appwork-css">
    <link rel="stylesheet" href="{{ mix('/vendor/css/rtl/theme-corporate.css') }}" class="theme-settings-theme-css">
    <link rel="stylesheet" href="{{ mix('/vendor/css/rtl/colors.css') }}" class="theme-settings-colors-css">
    <link rel="stylesheet" href="{{ mix('/vendor/css/rtl/uikit.css') }}">
    <link rel="stylesheet" href="{{ mix('/vendor/libs/bootstrap-sweetalert/bootstrap-sweetalert.css') }}">
    <link rel="stylesheet" href="{{ mix('/css/demo.css') }}">

    <script src="{{ mix('/vendor/js/material-ripple.js') }}"></script>
    <script src="{{ mix('/vendor/js/layout-helpers.js') }}"></script>

    <!-- Theme settings -->

    <!-- Core scripts -->
    <script src="{{ mix('/vendor/js/pace.js') }}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <!-- Libs -->
    <link rel="stylesheet" href="{{ mix('/vendor/libs/perfect-scrollbar/perfect-scrollbar.css') }}">

    @yield('styles')
</head>
<body>
<div class="page-loader">
    <div class="bg-primary"></div>
</div>

@yield('layout-content')

<!-- Core scripts -->
<script src="{{ mix('/vendor/libs/popper/popper.js') }}"></script>
<script src="{{ mix('/vendor/js/bootstrap.js') }}"></script>
<script src="{{ mix('/vendor/js/sidenav.js') }}"></script>

<!-- Scripts -->
<script src="{{ mix('/vendor/libs/perfect-scrollbar/perfect-scrollbar.js') }}"></script>
<script src="{{ mix('/vendor/libs/bootstrap-sweetalert/bootstrap-sweetalert.js') }}"></script>
<script src="{{ mix('/js/demo.js') }}"></script>

@yield('scripts')
</body>
</html>
