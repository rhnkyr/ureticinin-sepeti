<?php

Route::group(
    [
        'prefix' => 'auth',
    ],
    static function () {
        Route::post('login', 'Api\AuthController@login');
        Route::post('signup', 'Api\AuthController@signup');

        Route::group(
            [
                'middleware' => 'auth:api',
            ],
            static function () {
                Route::get('logout', 'Api\AuthController@logout');
                Route::get('me', 'Api\AuthController@user');
            }
        );
    }

);

Route::group(
    [
        'middleware' => 'auth:api',
    ],
    static function () {
        Route::resource('categories', 'Api\CategoryController');
        Route::resource('transports', 'Api\TransportationController');
        Route::resource('adverts', 'Api\AdvertController');
        Route::resource('advert-tags', 'Api\AdvertTagController');
        Route::resource('faqs', 'Api\FaqController');
        Route::resource('advert-comments', 'Api\AdvertCommentController');
        Route::resource('users', 'Api\UserController');
        Route::resource('static-contents', 'Api\StaticContentController');
        Route::resource('conversations', 'Api\ConversationController');
        Route::resource('messages', 'Api\MessageController');

        Route::resource('prices', 'Api\PriceController');
        Route::resource('user-settings', 'Api\UserSettingController');
        Route::resource('user-adverts', 'Api\UserAdvertController');

        Route::get('category-helper', 'Api\CategoryHelperController@index');
        Route::get('parent-categories', 'Api\CategoryHelperController@parentCategories');
        Route::get('advert-by-category/{id}', 'Api\AdvertHelperController@byCategory');
    }
);

Route::get('/', static function () {
    return view('welcome');
});
