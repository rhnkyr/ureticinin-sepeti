<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(
    [
        'prefix'     => 'admin',
        'middleware' => 'check_admin',
    ],
    static function () {
        Route::resource('categories', 'Admin\CategoryController');
        Route::resource('transports', 'Admin\TransportationController');
        Route::resource('adverts', 'Admin\AdvertController');
        Route::resource('faqs', 'Admin\FaqController');
        Route::resource('advert-comments', 'Admin\AdvertCommentController');
        Route::resource('users', 'Admin\UserController');
        Route::resource('static-contents', 'Admin\StaticContentController');
        Route::resource('prices', 'Admin\PriceController');

        Route::post('upload-image', 'Admin\UploadHelperController@upload')->name('upload-image');
        Route::post('delete-image', 'Admin\UploadHelperController@delete')->name('delete-image');
        Route::post('level-1', 'Admin\DataHelperController@options')->name('options');
    }
);


Route::post('/login', 'Common\LoginController@login')->name('login');
Route::get('/admin', 'Common\LoginController@index')->name('index');

//Route::get('/data', 'Common\LoginController@getData');
